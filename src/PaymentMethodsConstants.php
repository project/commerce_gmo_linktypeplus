<?php

namespace Drupal\commerce_gmo_linktypeplus;

/**
 * Payment Method constants.
 */
class PaymentMethodsConstants {
  const CREDIT = 0;
  const CVS = 3;
  const PAYEASY = 4;
  const DOCOMOPAY = 9;
  const SOFTBANK = 11;
  const BANK_TRANSFER = 36;
  const AU_PAY = 39;
  const FAMIPAY = 39;
  const EPOS = 40;
  const PAYPAY = 45;
  const RAKUTEN_PAY = 50;

}
